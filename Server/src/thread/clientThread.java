package thread;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import MD5.MD5;

import models.Account;

import constant.SystemConstants;
import file.FileManager;

public class clientThread extends Thread{
	
	private String clientName = null;
	private DataInputStream is = null;
	private PrintStream os = null;
	private Socket clientSocket = null;
	private final clientThread[] threads;
	private int maxClientsCount;
	public Account account = new Account();
	public boolean isSignIn = false;
	public ArrayList<String> listFileName = new ArrayList<String>();
	
	public clientThread(Socket clientSocket, clientThread[] threads) {
		this.clientSocket = clientSocket;
		this.threads = threads;
		maxClientsCount = threads.length;
	}
	/*
	 * receive account info when client execute sign in or sign up
	 */
	public void receiveAccountInfo(){
		try{
			String username = "";
			String password = "";
			username = is.readLine().trim(); // receive account name
			account.setAccountName(username);
			password = is.readLine().trim(); // receive password
			password = MD5.encryptMD5(password); // convert password by using MD5
			account.setPassword(password);
			System.out.println("Account = " + account.getAccountName() + " Password = " + account.getPassword());
		}catch(IOException e){
			
		}
	}
	/*
	 * receive list of file name when client sign in successfully
	 */
	public void receiveFileName(){
		try{
			boolean keepGoing = true;
        	ArrayList<String> lstFileName = new ArrayList<String>();
        	while (keepGoing){
        		try{
        			String received = is.readLine().trim();
        			if(received.equals(SystemConstants.FLAG_MESSAGE)){
        				break;
        			}
        			System.out.println("File: " + received);
        			lstFileName.add(received);
        		}catch (SocketTimeoutException ste){
        			keepGoing = false;
        		}
        	}
        	listFileName = lstFileName;
		}catch(IOException e){
			
		}
	}
	/*
	 * send file name to client when client want view all shared files
	 */
	public void sendFileName(){
		boolean result = false;
		synchronized (this){
			for(int i = 0; i < maxClientsCount; i++){
				if (threads[i] != null && threads[i] != this){
					for(int j = 0; j < threads[i].listFileName.size(); j++){
						os.println(threads[i].listFileName.get(j));
					}
					if(threads[i].listFileName.size() > 0){
						result = true;
					}
				}
			}
		}
		if(!result){
			os.println(SystemConstants.RESULT_VIEW);
		}else{
			os.println(SystemConstants.FLAG_MESSAGE);
		}
	}
	/*
	 * send file name to client when client execute search file name
	 */
	public void searchFileName(String filename){
		boolean result = false;
		synchronized (this){
			for(int i = 0; i < maxClientsCount; i++){
				if (threads[i] != null && threads[i] != this){
					for(int j = 0; j < threads[i].listFileName.size(); j++){
						if(threads[i].listFileName.get(j).contains(filename)){
							os.println(threads[i].listFileName.get(j));
							result = true;
						}
					}
					
				}
			}
		}
		if(!result){
			os.println(SystemConstants.RESULT_SEARCH);
		}else{
			os.println(SystemConstants.FLAG_MESSAGE);
		}
	}
	/*
	 * search file and send message to client have file
	 */
	public void downloadFileName(String filename){
		boolean result = false;
		synchronized (this){
			for(int i = 0; i < maxClientsCount; i++){
				if (threads[i] != null && threads[i] != this){
					for(int j = 0; j < threads[i].listFileName.size(); j++){
						if(threads[i].listFileName.get(j).equals(filename)){
							threads[i].os.println(clientName);
							result = true;
							break;
						}
					}
					if(result){
						break;
					}
				}
			}
		}
		if(!result){
			os.println(SystemConstants.RESULT_SEARCH);
		}
	}
	
	public void run(){
		int maxClientsCount = this.maxClientsCount;
		clientThread[] threads = this.threads;
		
		try{
			/*
		     * Create input and output streams for this client.
		     */
			is = new DataInputStream(clientSocket.getInputStream());
			os = new PrintStream(clientSocket.getOutputStream());
			String name;
			os.println(SystemConstants.CONNECT_SUCCESS);
			while(true){
				name = is.readLine().trim();
				if(name.equals(SystemConstants.SIGN_UP)){
					
					receiveAccountInfo();
					
					FileManager file = new FileManager();
					if(file.readFileSignUp(account)){
						os.println(SystemConstants.SIGNUP_SUCCESS);
						file.writeFile(account.getAccountName() + " " + account.getPassword());
					}else{
						os.println(SystemConstants.SIGNUP_FAIL);
					}
				}
				if(name.equals(SystemConstants.SIGN_IN)){
					boolean isTrue = true;
					receiveAccountInfo();
					
					synchronized (this) {
						for (int i = 0; i < maxClientsCount; i++) {
							if (threads[i] != null && threads[i] != this) {
								if(account.getAccountName().equals(threads[i].account.getAccountName()) && threads[i].isSignIn == true){
									isTrue = false;
									break;
								}
							}
						}
					}
					if(isTrue){
						FileManager file = new FileManager();
						if(file.readFileSignIn(account)){
							os.println(SystemConstants.SIGNIN_SUCCESS);
							isSignIn = true;
							receiveFileName();
							break;
						}else{
							os.println(SystemConstants.SIGNIN_FAIL);
						}
					}else{
						os.println(SystemConstants.SIGNIN_FAIL);
					}
				}
			}
			
			/* Welcome the new the client. */
			synchronized (this){
				for (int i = 0; i < maxClientsCount; i++) {
					if (threads[i] != null && threads[i] == this) {
						clientName = "@" + account.getAccountName() ;
						break;
					}
				}
			}
			/* Start the conversation. */
			while(true){
				String line = is.readLine();
				if(line.equals(SystemConstants.VIEW_FILE)){
					sendFileName();
				}else if(line.equals(SystemConstants.SEARCH_FILE)){
					line = is.readLine();
					searchFileName(line);
				}else if(line.equals(SystemConstants.DOWNLOAD_FILE)){
					line = is.readLine();
					downloadFileName(line);
				}else if(line.equals(SystemConstants.CLOSED_CONNECT)){
					break;
				}else{
					
				}
				/* If the message is private sent it to the given client. */
				if (line.startsWith("@")) {
					String[] words = line.split("\\s", 2);
					synchronized (this) {
						for (int i = 0; i < maxClientsCount; i++) {
							if (threads[i] != null && threads[i] != this
				                    && threads[i].clientName != null
				                    && threads[i].clientName.equals(words[0])) {
								threads[i].os.println(words[1]);
								break;
							}
						}
					}
				}
			}
			os.println(SystemConstants.QUIT);
			/*
			 * Clean up. Set the current thread variable to null so that a new client
			 * could be accepted by the server.
			 */
			synchronized (this) {
				for (int i = 0; i < maxClientsCount; i++) {
					if (threads[i] == this) {
						threads[i] = null;
					}
				}
			}
			/*
			 * Close the output stream, close the input stream, close the socket.
			 */
			is.close();
			os.close();
			clientSocket.close();
		}catch(IOException e){
			
		}
	}
}
