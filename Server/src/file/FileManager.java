package file;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import models.Account;

import constant.SystemConstants;

public class FileManager {
	
	public void writeFile(String content){
		content += "\n";
		File fnew=new File(SystemConstants.FILE_ACCOUNTS);
		try {
		    FileWriter f2 = new FileWriter(fnew, true);
		    f2.write(content);
		    f2.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}  
	}
	
	public boolean readFileSignIn(Account account){
		try {
			// Open the file that is the first 
			// command line parameter
			FileInputStream fstream = new FileInputStream(SystemConstants.FILE_ACCOUNTS);
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			//Read File Line By Line
			while ((strLine = br.readLine()) != null)   {
				String[] info = strLine.split("\\s");
				if (account.getAccountName().equals(info[0]) && account.getPassword().equals(info[1])){
					in.close();
					return true;
				}
			}
			//Close the input stream
			in.close();
			return false;
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			return false;
		}
	}
	
	public boolean readFileSignUp(Account account){
		try {
			// Open the file that is the first 
			// command line parameter
			FileInputStream fstream = new FileInputStream(SystemConstants.FILE_ACCOUNTS);
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			//Read File Line By Line
			while ((strLine = br.readLine()) != null)   {
				String[] info = strLine.split("\\s");
				if (account.getAccountName().equals(info[0])){
					in.close();
					return false;
				}
			}
			//Close the input stream
			in.close();
			return true;
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			return false;
		}
	}
}
