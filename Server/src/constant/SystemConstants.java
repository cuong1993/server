package constant;

public interface SystemConstants {
    public static final int MAX_CLIENTS = 10;
    public static final int PORT_NUMBER = 2222;
    
    public static final String CONNECT_SUCCESS = "Ket noi Server thanh cong !!!!!";
    public static final String SIGN_UP = "sign_up";
    public static final String SIGN_IN = "sign_in";
    public static final String FILE_ACCOUNTS = "account.txt";
    
    public static final String SIGNIN_SUCCESS = "=> Dang nhap thanh cong";
    public static final String SIGNIN_FAIL = "=> Dang nhap that bai";
    
    public static final String SIGNUP_SUCCESS = "=> Dang ky thanh cong";
    public static final String SIGNUP_FAIL = "Tai khoan da ton tai. Xin ban vui long dang ky lai.";
    
    public static final String FLAG_MESSAGE = "-1";
    
    public static final String VIEW_FILE = "xem";
    public static final String RESULT_VIEW = "0";
    public static final String RESULT_SEARCH = "00";
    
    public static final String SEARCH_FILE = "tim";
    public static final String DOWNLOAD_FILE = "download";
    public static final String CLOSED_CONNECT = "close";
    public static final String QUIT = ">> Dong ket noi voi Server >>";
}
